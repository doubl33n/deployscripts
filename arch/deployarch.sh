#!/bin/sh

# Arch Linux deployment script
# This script can be run from the live ISO. If you choose to install BSPWM, it will use my dotfiles for your system.


#**************************************
#   First of all, check for Arch
#


distro=$(cat /etc/os-release | grep ID=arch$)

if [[ -z "$distro" ]]; then
    echo "You are not running this script on Arch Linux. Script will terminate."
    exit 1
fi


#**************************************
#   Now some initial setup
#


origin_directory=$(pwd)
install_profile=0
partition_scheme=0
swapsize="4G"
do_swap="n"
target_device=""
target_partition=""

get_confirmation() {
    read -n 1 confirmation
    if ! [[ "$confirmation" == "y" ]]; then
        echo "Program aborted."
        exit 1
    fi
}

get_choice() {
    read -n 1 choice
        echo "Program aborted."
    if ! [[ "$choice" == "y" ]] && ! [[ "$choice" == "n" ]]; then
        exit 1
    elif [[ "$choice" == "y" ]]; then
        return
    fi
}



#**************************************
#   Start the script
#


echo ""
echo "Welcome to my install script for Arch Linux!"
echo "--------------------------------------------"
echo ""


#**************************************
#   Getting required files
#


if ! [ -x "$(command -v git)" ]; then
    echo "Git is not installed. Installing it now"
    pacman -Sy
    pacman -S git --noconfirm
fi

if ! [[ "../home/README.md" == "" ]] && ! [[ -d "my_dotfiles" ]]; then
    echo "Downloading dotfiles from repo"
    git clone https://gitlab.com/doubl33n/my_dotfiles.git
    cd my_dotfiles/
else
    if [[ -d "my_dotfiles" ]]; then
        cd my_dotfiles
    else
        cd ../
    fi
fi

timedatectl set-ntp true

echo "Running from folder $(pwd)"
echo ""


#**************************************
#   HDD Setup
#


while [[ "$target_device" == "" ]]; do
    echo "First of all, where do you want to install your OS?"
    echo ""
    echo "$(lsblk)"
    echo ""
    echo "Please select a disk, NOT a partition (for example /dev/sda)"
    read target_device
    if [ ! -b "$target_device" ]; then
        echo "Disk does not exist, please try again"
        target_device=""
    fi
done

echo ""
echo "---------------------------------"
echo ""

while ! [[ $partition_scheme =~ ^[0-9]+$ ]] || [[ $partition_scheme -gt 4 || $partition_scheme -lt 1 ]]; do
    echo "Please select the partitioning scheme:"
    echo "1: Full disk erase & use as /"
    echo "2: Full disk erase, seperate /boot partition"
    echo "3: Use partition as /"
    echo "4: Custom scheme"
    echo ""
    echo "Please choose your scheme"
    read -n 1 partition_scheme
    echo ""
    if ! [[ $partition_scheme =~ ^[0-9]+$ ]] || [[ $partition_scheme -gt 4 || $partition_scheme -lt 1 ]]; then
        echo "Selected partition_scheme is invalid, please try again"
    fi
done

echo "Do you want to have a swap partition? Press y if that's the case."
read -n 1 do_swap
if [[ $do_swap="y" ]]; then
    echo ""
    echo "Setting swap partition size to 4GiB. If you want a different size, press n. Otherwise, press y."
    get_choice
    while ! [[ $swapsize =~ ^([0-9]{1,3})+([GM])$ ]]; do
        echo ""
        echo "How large should your swap partition be?"
        echo "Examples: 512M or 4G or 8G"
        read swapsize -n 5
        if ! [[ $swapsize =~ ^([0-9]{1,3})+([GM])$ ]]; then
            echo "Invalid size, please try again"
        fi
    done
    echo "Swap size will be $swapsize. Is that okay?"
    get_confirmation
fi

if [[ $partition_scheme -eq 3 ]]; then
    while [ ! -b "$target_partition" ]; do
        echo "Which partition do you want to use?"
        echo ""
        echo "$(lsblk $target_device)"
        echo ""
        echo "Please select a partition, NOT a disk (for example ${target_device}2)"
        read target_partition
        if [ ! -b "$target_partition" ]; then
            echo "Partition does not exist, please try again"
            target_partition=""
        fi
    done
    echo "Once you're ready to proceed, press y."
    get_confirmation
fi

if [[ $partition_scheme -eq 4 ]]; then
    echo "Please partition the disk yourself and then return, if you haven't already."
    echo "Once you're ready to proceed, please start the script and choose option #3."
    exit 1
fi


echo ""
echo "---------------------------------"
echo ""

echo "Selected partitioning scheme: $partition_scheme"


echo "The script will now apply your chosen profile to the disk $target_device. Is that okay? (y/n)"
get_confirmation
case $partition_scheme in
    1) 
        if [[$do_swap = "y"]]; then
            sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << FDISK_CMDS  | sudo fdisk --wipe $target_device
o      # create new MSDOS partition
n      # add new partition
p      # primary partition
1      # partition number
       # default - first sector 
+$swapsize # default - last sector 
t      # change partition type
82     # Linux filesystem
n      # add new partition
p      # primary partition
2      # partition number
       # default - first sector 
       # default - last sector 
t      # change partition type
2      # partition number
83     # Linux filesystem
a      # Set bootable flag
2      # For partition 2
w      # write partition table and exit

FDISK_CMDS
            mkswap "${target_device}1"
            mkfs.ext4 "${target_device}2"
            target_partition="${target_device}2"
        else    
            sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << FDISK_CMDS  | sudo fdisk --wipe $target_device
o      # create new MSDOS partition
n      # add new partition
p      # primary partition
1      # partition number
       # default - first sector 
       # default - last sector 
t      # change partition type
83     # Linux filesystem
a      # Set bootable flag
w      # write partition table and exit
FDISK_CMDS
            mkfs.ext4 "${target_device}1"
            target_partition="${target_device}1"
        fi
    ;;

    2) 
       sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << FDISK_CMDS  | sudo fdisk --wipe $target_device
o      # create new MSDOS partition
n      # add new partition
p      # primary partition
1      # partition number
       # default - first sector 
+1G    # default - last sector 
t      # change partition type
83     # Linux filesystem
a      # Set bootable flag
n      # add new partition
p      # primary partition
2      # partition number
       # default - first sector 
       # default - last sector 
t      # change partition type
2      # partition number
83     # Linux filesystem
w      # write partition table and exit
FDISK_CMDS
       mkfs.ext4 -T small "${target_device}1"
       mkfs.ext4 "${target_device}2"
       target_partition="${target_device}2"
    ;;
        
    3)

    ;;
       
    4)

    ;;

    *) echo "INVALID SCHEME!" ; exit 1; ;;
esac

echo ""
echo "Done! Here's your current layout."
echo "$(lsblk)"
echo ""



#**************************************
#   Choosing a profile
#


echo ""
echo "---------------------------------"
echo ""

while ! [[ $install_profile =~ ^[0-9]+$ ]] || [[ $install_profile -gt 6 || $install_profile -lt 1 ]]; do
    echo "Now we'll choose what software you want to install."
    echo ""
    echo "This script will offer you the following options:"
    echo "1: minimal, no GUI"
    echo "2: minimal, bspwm+rice"
    echo "3: minimal, bspwm"
    echo "4: Full install, bspwm+rice & different desktop environment"
    echo "5: Full install, different desktop environment"
    echo "6: Manually select software"
    echo ""
    echo "Please choose your profile"
    read -n 1 install_profile
    echo ""
    if ! [[ $install_profile =~ ^[0-9]+$ ]] || [[ $install_profile -gt 6 || $install_profile -lt 1 ]]; then
        echo "Selected profile is invalid, please try again"
    fi
done

echo ""
echo "---------------------------------"
echo ""

#**************************************
#   Final setup
#





echo "Arch Linux will be installed to $target_partition using Profile $install_profile now. If that is okay, please enter 'y'."
get_confirmation

echo ""
echo "---------------------------------"
echo ""

case $install_profile in
    1) 
        echo "You selected profile 1"
    ;;

    2) 

    ;;
        
    3)

    ;;
        
    4)

    ;;

    5)

    ;;
    
    6)

    ;;
        
    *) echo "INVALID PROFILE!" ; exit 1; ;;
esac


